package com.anthology.json;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.util.ArrayList;



public class PojoToJson
{
    public PojoToJson() {}

    public static void main(String[] args)
    {
        User a = new User(23, "amal");
        ArrayList<String> message = new ArrayList();
        message.add("m1");
        message.add("m2");
        a.setMessages(message);

        User b = new User(58, "pete");
        User[] ab = { a, b };

        ObjectMapper mapper = new ObjectMapper();
        try
        {
            String s1 = getJson1(a);
            System.out.println(s1);
            User user1 = (User)mapper.readValue(s1, User.class);
            System.out.println(user1);

            System.out.println("----------------");

            String s2 = getJson2(ab);
            System.out.println(s2);
            User[] user2 = (User[])mapper.readValue(s2, User[].class);
            for (User u : user2) {
                System.out.println(u);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getJson1(User user) throws IOException
    {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(user);
    }

    private static String getJson2(User[] ab) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(ab);
    }
}

@JsonSerialize
class User
{
    @JsonProperty
    private int age;
    @JsonProperty
    private String name;
    @JsonProperty
    private ArrayList<String> messages;

    public User() {}

    public User(int age, String name)
    {
        this();
        this.age = age;
        this.name = name;
    }

    public void setMessages(ArrayList<String> messages)
    {
        this.messages = messages;
    }

    public int getAge()
    {
        return this.age;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public ArrayList<String> getMessages()
    {
        return this.messages;
    }

    public String toString()
    {
        return "User{age=" + this.age + ", name='" + this.name + '\'' + ", messages=" + this.messages + '}';
    }
}


