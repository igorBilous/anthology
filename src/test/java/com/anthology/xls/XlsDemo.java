package com.anthology.xls;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.ss.usermodel.CellStyle;

import java.io.FileOutputStream;

public class XlsDemo {
    public XlsDemo() {
    }

    @org.junit.Test
    public void main() {
        try {
            String filename = "c:/hello.xls";
            org.apache.poi.hssf.usermodel.HSSFWorkbook hwb = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
            org.apache.poi.hssf.usermodel.HSSFSheet sheet = hwb.createSheet("new sheet");
            HSSFRow rowhead = sheet.createRow(0);
            HSSFCell cell1 = rowhead.createCell(0);
            CellStyle cs = hwb.createCellStyle();
            cs.setFillBackgroundColor((short) 10);
            cell1.setCellStyle(cs);
            cell1.setCellValue("SNo");
            rowhead.createCell(1).setCellValue("First Name");
            rowhead.createCell(2).setCellValue("Last Name");
            HSSFCell cell = rowhead.createCell(3);
            cell.setCellValue("Username");
            rowhead.createCell(4).setCellValue("E-mail");
            rowhead.createCell(5).setCellValue("Country");
            HSSFRow row = sheet.createRow(1);
            row.createCell(0).setCellValue("1");
            row.createCell(1).setCellValue("Rose");
            row.createCell(2).setCellValue("India");
            row.createCell(3).setCellValue("roseindia");
            row.createCell(4).setCellValue("hello@roseindia.net");
            row.createCell(5).setCellValue("India");
            FileOutputStream fileOut = new FileOutputStream(filename);
            hwb.write(fileOut);
            fileOut.close();
            System.out.println("Your excel file has been generated!");
        } catch (Exception ex) {
            System.out.println(ex);
        }
    }


    @org.junit.Test
    public void testColumnWithWidth()
            throws Exception {
        FileOutputStream createExcel = new FileOutputStream("C:/workbook.xls");
        org.apache.poi.hssf.usermodel.HSSFWorkbook workbook = new org.apache.poi.hssf.usermodel.HSSFWorkbook();
        org.apache.poi.hssf.usermodel.HSSFSheet createSheet = workbook.createSheet("sheet one");
        HSSFRow row0 = createSheet.createRow(0);
        HSSFCell cell0 = row0.createCell(0);
        cell0.setCellValue("apache poi excel manipulation");
        createSheet.autoSizeColumn(0);
        workbook.write(createExcel);
        createExcel.flush();
        createExcel.close();
    }
}
