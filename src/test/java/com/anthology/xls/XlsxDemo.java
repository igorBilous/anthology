package com.anthology.xls;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.*;

import java.io.FileOutputStream;
import java.io.IOException;

public class XlsxDemo
{
    public XlsxDemo() {}

    @org.junit.Test
    public void main() throws IOException
    {
        Workbook wb = new XSSFWorkbook();
        Sheet sheet = wb.createSheet("Fonts");

        Font font0 = wb.createFont();
        font0.setColor(IndexedColors.BROWN.getIndex());
        CellStyle style0 = wb.createCellStyle();
        style0.setFont(font0);

        Font font1 = wb.createFont();
        font1.setFontHeightInPoints((short)14);
        font1.setFontName("Courier New");
        font1.setColor(IndexedColors.RED.getIndex());
        CellStyle style1 = wb.createCellStyle();
        style1.setFont(font1);

        Font font2 = wb.createFont();
        font2.setFontHeightInPoints((short)16);
        font2.setFontName("Arial");
        font2.setColor(IndexedColors.GREEN.getIndex());
        CellStyle style2 = wb.createCellStyle();
        style2.setFont(font2);

        Font font3 = wb.createFont();
        font3.setFontHeightInPoints((short)18);
        font3.setFontName("Times New Roman");
        font3.setColor(IndexedColors.LAVENDER.getIndex());
        CellStyle style3 = wb.createCellStyle();
        style3.setFont(font3);

        Font font4 = wb.createFont();
        font4.setFontHeightInPoints((short)18);
        font4.setFontName("Wingdings");
        font4.setColor(IndexedColors.GOLD.getIndex());
        CellStyle style4 = wb.createCellStyle();
        style4.setFont(font4);

        Font font5 = wb.createFont();
        font5.setFontName("Symbol");
        CellStyle style5 = wb.createCellStyle();
        style5.setFont(font5);

        Cell cell0 = sheet.createRow(0).createCell(1);
        cell0.setCellValue("Default");
        cell0.setCellStyle(style0);

        Cell cell1 = sheet.createRow(1).createCell(1);
        cell1.setCellValue("Courier");
        cell1.setCellStyle(style1);

        Cell cell2 = sheet.createRow(2).createCell(1);
        cell2.setCellValue("Arial");
        cell2.setCellStyle(style2);

        Cell cell3 = sheet.createRow(3).createCell(1);
        cell3.setCellValue("Times New Roman");
        cell3.setCellStyle(style3);

        Cell cell4 = sheet.createRow(4).createCell(1);
        cell4.setCellValue("Wingdings");
        cell4.setCellStyle(style4);

        Cell cell5 = sheet.createRow(5).createCell(1);
        cell5.setCellValue("Symbol");
        cell5.setCellStyle(style5);


        FileOutputStream fileOut = new FileOutputStream("C:/xssf-fonts.xlsx");
        wb.write(fileOut);
        fileOut.close();
    }

    @org.junit.Test
    public void colorStyleSheet2() throws IOException {
        FileOutputStream fileOut = new FileOutputStream("C:/test.xlsx");
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = workbook.createSheet();
        XSSFRow row = sheet.createRow(0);
        row.setHeightInPoints(50.0F);
        XSSFCell cell = row.createCell(0);
        cell.setCellValue("custom XSSF colors");

        XSSFCellStyle style1 = workbook.createCellStyle();
        style1.setFillForegroundColor(new org.apache.poi.xssf.usermodel.XSSFColor(new java.awt.Color(218, 225, 226)));
        style1.setFillPattern((short)1);


        org.apache.poi.xssf.usermodel.XSSFFont font = workbook.createFont();
        font.setFontName("Times New Roman");
        font.setBoldweight((short)700);
        style1.setFont(font);
        workbook.write(fileOut);
        fileOut.flush();
        fileOut.close();
    }

    @org.junit.Test
    public void colorStyleSheet() throws IOException
    {
        try {
            FileOutputStream fileOut = new FileOutputStream("C:/test.xlsx");
            XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet worksheet = workbook.createSheet("Worksheet");
            XSSFSheet worksheet2 = workbook.createSheet("Worksheet2");

            XSSFRow row = worksheet.createRow(0);

            XSSFCell cellA1 = row.createCell(0);
            cellA1.setCellValue("Hello");

            XSSFCellStyle styleOfCell = workbook.createCellStyle();
            styleOfCell.setFillForegroundColor((short)49);
            styleOfCell.setFillPattern((short)1);
            cellA1.setCellStyle(styleOfCell);

            XSSFCell cellB1 = row.createCell(1);
            cellB1.setCellValue("World");
            styleOfCell = workbook.createCellStyle();
            styleOfCell.setFillForegroundColor((short)49);
            styleOfCell.setFillPattern((short)1);
            cellB1.setCellStyle(styleOfCell);

            XSSFCell cellC1 = row.createCell(2);
            cellC1.setCellValue("Happy");
            styleOfCell = workbook.createCellStyle();

            styleOfCell.setFillForegroundColor(new org.apache.poi.xssf.usermodel.XSSFColor(new java.awt.Color(255, 255, 255)));

            styleOfCell.setFillPattern((short)1);
            cellC1.setCellStyle(styleOfCell);

            XSSFCell cellD1 = row.createCell(3);
            cellD1.setCellValue(new java.util.Date());
            styleOfCell = workbook.createCellStyle();
            styleOfCell.setDataFormat(org.apache.poi.hssf.usermodel.HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));

            styleOfCell.setFillForegroundColor((short)49);
            styleOfCell.setFillPattern((short)1);
            cellD1.setCellStyle(styleOfCell);

            row = worksheet.createRow(1);
            styleOfCell.setFillForegroundColor(new org.apache.poi.xssf.usermodel.XSSFColor(new java.awt.Color(255, 255, 255)));
            org.apache.poi.xssf.usermodel.XSSFFont font = workbook.createFont();
            font.setFontName("Times New Roman");
            styleOfCell.setFont(font);
            row.setRowStyle(styleOfCell);
            row.createCell(0).setCellValue(java.util.Calendar.getInstance().getTime().toString());
            row.createCell(1).setCellValue("a string");
            row.createCell(2).setCellValue(true);
            row.createCell(3).setCellType(5);







            workbook.write(fileOut);
            fileOut.flush();
            fileOut.close();
        } catch (java.io.FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
