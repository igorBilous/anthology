package com.anthology;

import java.util.Arrays;

public class FullCoverage {

    public static void main(String[] args) {
        String[] arr1 = new String[] { "3", "4" };
        String[] arr2 = new String[] { "7", "8" };
        String[] arr3 = new String[] { "9", "1" };

        String[] res = new String[3];

        print(0, res, arr1, arr2, arr3);
    }

    private static void print(int index, String[] res, String[]... arrays) {
        if (index == res.length) {
            System.out.println(Arrays.toString(res));
            return;
        }

        String[] arr = arrays[index];

        for (int j = 0; j < arr.length; j++) {
            res[index] = arr[j];

            print(index + 1, res, arrays);
        }
    }
}
