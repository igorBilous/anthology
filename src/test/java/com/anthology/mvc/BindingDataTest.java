package com.anthology.mvc;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration({"file:src/main/resources/root.xml"})
public class BindingDataTest
{
    private MockMvc mockMvc;
    @Autowired
    protected WebApplicationContext wac;

    public BindingDataTest() {}

    @Before
    public void setup()
    {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    @Test
    public void testBindingData() throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/?a=21&b=s&b=asd", new Object[0])).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.view().name("bla")).andReturn();
    }


    @Test
    public void IsBooleanParamBindRight()
            throws Exception
    {
        this.mockMvc.perform(MockMvcRequestBuilders.post("/bindBoolean?boolean=false", new Object[0])).andExpect(MockMvcResultMatchers.status().isOk()).andExpect(MockMvcResultMatchers.view().name("bla")).andReturn();
    }

}
