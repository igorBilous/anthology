package com.anthology.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * User: igorb
 * Date: 09.09.13  Time: 10:44
 */
@Controller
@RequestMapping("/")
public class BindingData {


    @RequestMapping(method = RequestMethod.POST)
    public String bindingData(Pojo pojo) {
        System.out.println(pojo.getA());
        System.out.println(Arrays.toString(pojo.getB()));
        return "bla";
    }

    @RequestMapping(value = "bindBoolean",method = RequestMethod.POST)
    public String bindingBooleanData(@RequestParam(value = "boolean", defaultValue = "false", required = false) Boolean bool) {
        System.out.println(bool);
        return "bla";
    }

    @RequestMapping(value = "largeData",method = RequestMethod.POST)
    public String receiveLargeData( HttpServletRequest request) {
        System.out.println("data length = "+request.getParameter("data").length());
        return "bla";
    }
}

class Pojo {
    String a;
    String[] b;

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public String[] getB() {
        return b;
    }

    public void setB(String[] b) {
        this.b = b;
    }
}
