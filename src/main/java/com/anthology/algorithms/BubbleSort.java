package com.anthology.algorithms;

import java.util.Arrays;

/**
 * Created by Svieta on 30.03.14.
 */
public class BubbleSort {

    public static void main(String args[]) {
        int[] unsorted = {32, 39, 21, 45, 23, 3, 4, 6, 33, 22};
        bubbleSort2(unsorted);
        int[] test = {5, 3, 2, 1};
        bubbleSort2(test);

    }

    public static void bubbleSort2(int[] array){
        for(int i = 0; i<array.length;i++){
            for(int j = 1; j<array.length-i;j++){
                if(array[j-1]>array[j]){
                    int temp = array[j];
                    array[j] = array[j-1];
                    array[j-1] = temp;
                }
            }

        }
        System.out.printf(Arrays.toString(array));
    }
}
