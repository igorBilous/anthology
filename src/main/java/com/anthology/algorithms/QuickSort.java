package com.anthology.algorithms;

import java.util.Arrays;
import java.util.Random;


public class QuickSort {

    public static int ARRAY_LENGTH = 31;
    private static int[] array = new int[ARRAY_LENGTH];
    private static Random generator = new Random();

    public static void initArray() {
        for (int i = 0; i < array.length; i++) {
            array[i] = generator.nextInt(100);
        }
    }

    public static void printArray() {
        System.out.println(Arrays.toString(array));
    }

    public static void quickSort() {
        int startIndex = 0;
        int endIndex = ARRAY_LENGTH - 1;
        doSort2(startIndex, endIndex);
    }

    private static void doSort2(int start, int end) {
        if(start>=end){
            return;
        }
        int i = start;
        int j = end;
        int pivot = i - (i - j) / 2;

        while (i < j) {
            while (i < pivot && array[i] <= array[pivot]) {
                i++;
            }
            while (j > pivot && array[j] >= array[pivot]) {
                j--;
            }
            if(i<j){
                int temp = array[i];
                array[i] = array[j];
                array[j] = temp;
                if (i == pivot)
                    pivot = j;
                else if (j == pivot)
                    pivot = i;
            }

        }
        doSort2(start, pivot);
        doSort2(pivot + 1, end);
    }

    public static void main(String[] args) {
        initArray();
        printArray();
        quickSort();
        printArray();
    }


}
