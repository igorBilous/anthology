package com.anthology.owner;

/**
 * User: anthology
 * Date: 31.10.13  Time: 14:43
 */
@org.aeonbits.owner.Config.Sources("classpath:config.properties")
public interface Config extends org.aeonbits.owner.Config {

    @Key("bla")
    int getBla();

    @Key("myType")
    ConfigType getMyType();
}
