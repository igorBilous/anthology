package com.anthology.owner;

/**
 * User: anthology
 * Date: 31.10.13  Time: 14:46
 */
public class ConfigType {

    String a;
    int b;

    public ConfigType(String a, int b) {
        this.a = a;
        this.b = b;
    }


    public ConfigType(String a) {
        this.a = a;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "ConfigType{" +
                "b=" + b +
                ", a='" + a + '\'' +
                '}';
    }
}
